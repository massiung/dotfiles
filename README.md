# Personal setup

Setup scripts for my personal setup:

- macOS software, settings and directory structure
- Terminal (zsh), vim, and tmux setup

## Terminal setup: iTerm2 + zsh + oh-my-zsh

<img src="docs/figures/example_iterm_terminal.png" width = "400px">
<img src="docs/figures/example_vim.png" width = "400px">

My main working station is a mac. I use the iTerm2 app with material colour scheme and Meslo nerd font.
Instead of bash I use zsh (pronounced z-shell, and comes preinstalled with macOS) together with the excellent oh-my-zsh
to manage themes, plugins, settings and aliases. zsh is posis compliant and also the same as bash, but offers modern things like tab-completion.

Best way to install everything is using my install script (run from default `Terminal.app` with bash):

```bash
install_scripts/setup_macOS.sh 
install_scripts/setup_macOS_terminal.sh 
```

## Todo

- anaconda setup
- tmux plugins and shortcuts
- rstudio configuration
