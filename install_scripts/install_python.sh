#!/bin/bash

# https://docs.brew.sh/Homebrew-and-Python

echo "Install miniconda."
brew cask install miniconda 

echo 'add line to bashrc: export PATH=/usr/local/miniconda3/bin:"$PATH"'

echo "Installing Python packages..."
PYTHON_PACKAGES=(
    ipython
    virtualenv
    virtualenvwrapper
)
pip3 install ${PYTHON_PACKAGES[@]}
