#!/bin/bash

files_array=$(ls -A ../files)

for file in ${files_array}
do
    if [ -f ${HOME}/${file} ]; then
        read -p "File ${HOME}/${file} already exists. Do you want to keep a backup? (y/N)" -n 1 answer
        if [[ ${answer} == "y" || ${answer} == "Y" ]]; then
          echo "Creating backup: ${HOME}/${file}.copy"
          mv ${HOME}/${file} ${HOME}/${file}.copy
        fi
        rm ${HOME}/${file}
    fi

    file_dir=$(eval "cd ../files;pwd")
    filepath="${file_dir}/${file}"
    echo "symlinking: ${filepath} to ${HOME}/.${file}"
    ln -fs ${filepath} ${HOME}/.${file}
done

echo "Installed dotfiles. Make sure to commit changes when you make them."
