#!/usr/bin/env bash
#
# Bootstrap script for setting up a new OSX machine

# Credits: Partly adapted from https://gist.github.com/codeinthehole/26b37efa67041e1307db
#
# This should be idempotent so it can be run multiple times.
#
echo "Starting setup of macOS"

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
    echo "Installing homebrew..."
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update homebrew recipes
brew update

# Install GNU core utilities (those that come with OS X are outdated)
brew tap homebrew/dupes
brew install coreutils
brew install gnu-sed --with-default-names
brew install gnu-tar --with-default-names
brew install gnu-indent --with-default-names
brew install gnu-which --with-default-names
brew install gnu-grep --with-default-names

# Install GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
brew install findutils

# The much faster `ag` command
brew install the_silver_searcher

# The very cool `exa`, a colorized `ls`
# Note I have an alias setup in my .zshrc file but not in .bashrc
brew install exa

# We need vim with clipboard support 
brew install vim --with-client-server

# Install some brew packages
echo "Installing packages..."
PACKAGES=(
  bash
  bash-completion
  zsh
  git
  jq
  tmux
  tree
  wget
  bat
  rmtrash
  reattach-to-user-namespace
)
brew install ${PACKAGES[@]}

echo "Cleaning up..."
brew cleanup

# Get homebrew cask
brew tap homebrew/cask

# Cask programs
CASKS=(
    dropbox
    flux
    google-chrome
    iterm2
    skype
    shiftit # simpler than amethyst
    vlc
    steam
    transmission
    pycharm-ce
)

echo "Installing cask apps..."
brew cask install ${CASKS[@]}

echo "Installing fonts..."
brew tap caskroom/fonts
FONTS=(
    font-roboto
    font-clear-sans
    font-fira-code
    font-meslo-nerd-font-mono
    font-meslo-nerd-font
)
brew cask install ${FONTS[@]}

echo "Configuring OSX..."

# Require password as soon as screensaver or sleep mode starts
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0

# Show filename extensions by default
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Make sure to not get those annoying .DS_Store files everywhere
defaults write com.apple.desktopservices DSDontWriteNetworkStores true

# Show hidden files by default
defaults write com.apple.finder AppleShowAllFiles YES

# Enable tap-to-click
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
# defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

# Disable "natural" scroll
# defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

# Setup my workspace folder
echo "Creating ~/workspace structure..."
[[ ! -d workspace ]] && mkdir workspace

# Some repo's I want cloned locally
git clone https://github.com/timvink/timvink.github.io.git

echo "... Done."

# Google chrome extensions
# j k  navigations
# Setup https://github.com/infokiller/google-search-navigator 

# System Preferences
#   - General
#     - Use dark menubar and doc
#   - Dock
#     - Automatically hide and show the dock
#   - Mission Control
#     - no Automatically rearrange spaces based on recent use
#   - Date & Time
#     - Clock
#       - Show date
#     - Keyboard
#       - Key repeat
#         - Fast
#       - Delay unt il repeat
#         - Short
# Menu Bar
#   - Battery Icon
#     - Show Percentage
